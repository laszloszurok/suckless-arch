static const char norm_fg[] = "#e0dbd2";
static const char norm_bg[] = "#191b28";
static const char norm_border[] = "#333";

static const char sel_fg[] = "#e0dbd2";
static const char sel_bg[] = "#563d7c";
static const char sel_border[] = "#663d9c";

static const char not_sel_fg[] = "#e0dbd2";
static const char not_sel_bg[]  = "#3e4050";
static const char not_sel_border[] = "#3c3933";

static const char hid_fg[] = "#e0dbd2";
static const char hid_bg[] = "#191b28";
static const char hid_border[] = "#000";

static const char urg_fg[] = "#e0dbd2";
static const char urg_bg[] = "#e05D6A";
static const char urg_border[] = "#e05D6A";
